import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { UUID } from 'angular2-uuid';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  
  private addedPatient=[];
  private patientUpdated = new Subject();
  updatedPatient;
  private _subject = new Subject<any>();

  constructor() { }

  getAllpatientList(){
    return [...this.addedPatient]
  }

  getPatientupdateListener() {
     return this.patientUpdated.asObservable();
  }

  addPatient(patientData){
    patientData.id = UUID.UUID();
    // this.postStatus = JSON.parse(localStorage.getItem('postcontent')) || [];
    this.addedPatient.push(patientData)
    console.log(patientData)
    this.patientUpdated.next([...this.addedPatient]);
    // localStorage.setItem('postcontent', JSON.stringify(this.postStatus))
  }



  // updatePatient(selectedItem){
  //    console.log(selectedItem)
  //    this.updatedPatient = selectedItem; 
  // }

  updatePatient(patientData) {
    console.log(patientData)
    
    for (const patient of this.addedPatient) {
      if (patient.id == patientData.id) {
        patient.address1 = patientData.address1
        patient.address2 = patientData.address2
        patient.city = patientData.city
        patient.email = patientData.email
        patient.lastName = patientData.lastName
        patient.phone = patientData.phone
        patient.pin = patientData.pin
        patient.userName = patientData.userName
        patient.zip = patientData.zip
      }
    }
   // this.patientUpdated = selectedItem;
  }

  updatePatientList(patientData,id){
    this.addedPatient.push(id,patientData.value)
  }

  newEvent(event) {
    this._subject.next(event);
  }

  get events$ () {
    return this._subject.asObservable();
  }
}
