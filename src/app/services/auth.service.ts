import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public isLoggedIn$: BehaviorSubject<boolean>;

  constructor(private router: Router) { 
    const isLoggedIn = localStorage.getItem('loggedIn') === 'true';
    this.isLoggedIn$ = new BehaviorSubject(isLoggedIn);
  }

  login() {
    // logic
    localStorage.setItem('loggedIn', 'true');
    this.isLoggedIn$.next(true);
  }

  logout() {
    // logic
    localStorage.removeItem('loggedIn')
    
   // this.isLoggedIn$.next(false);
            // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'])
  }
  

}